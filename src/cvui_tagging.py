import cv2
import numpy as np
import sqlite3
import io
from PIL import Image


path = r"data/20231012_tagging_db.sqlite"

n_cols = 12
n_imgs_max = n_cols*8


def get_rows(select_sql = "SELECT * FROM observation_tagging;",
                    sqlite_file = None):
    if not sqlite_file:
        sqlite_file = path

    cols = 'obs_id fname image tags'.split()

    try:
        con = sqlite3.connect(sqlite_file)
        
        with con:
            cursor = con.cursor()
            cursor.execute(select_sql)
            rows = cursor.fetchall()
            rows = [dict(zip(cols, i)) for i in rows]
            cursor.close()
            return rows
        
    finally:
        if 'con' in locals() and con:
            con.close()

def update_rows(rows, sql = "UPDATE observation_tagging SET tags = ? WHERE obs_id = ?",
                    sqlite_file = None):
    

    if not sqlite_file:
        sqlite_file = path

    recordList = [(dc['tags'], dc['obs_id']) for dc in rows]

    try:
        con = sqlite3.connect(sqlite_file)
        with con:
            cursor = con.cursor()
            cursor.executemany(sql, recordList)
            cursor.close()
            con.commit()
        
    finally:
        if 'con' in locals() and con:
            con.close()
            


rows = get_rows()

with open('outfile.txt', 'w') as fp:
    print({r['obs_id']:r['tags'] for r in rows}, file=fp)


for i in range(len(rows)):
    rows[i]['image'] = np.array(Image.open(io.BytesIO(rows[i]['image'])), dtype=np.uint8)

def toggle_bad_good(event,x,y, flags,param):
    global full_img, rows, lut, slices

    if event == cv2.EVENT_LBUTTONDBLCLK:
        x0, y0 = int(x), int(y)
        print(x0, y0)
        i = lut[y0, x0]
        if i >= 0:
            r = rows[i]
            sx, sy = slices[i]
            if 'bad' in r['tags']:
                print(i, 'bad -> good')
                r['tags'] = r['tags'].replace('bad', 'good')
            elif 'good' in r['tags']:
                print(i, 'good -> bad')
                r['tags'] = r['tags'].replace('good', 'bad')

            rows[i] = r
            full_img[sx, sy] = make_color(rows[i])
            # print(np.mean(img[:,:,0]), np.mean(img[:,:,1]), np.mean(img[:,:,2]))


def make_color(r):
    tags = r['tags']
    img = r['image']
    obs_id = r['obs_id']

    if 'good' in tags:
        color = 1
    elif 'bad' in tags:
        color = 2
    else:
        color = 0
    i = np.zeros((img.shape[0], img.shape[1], 3), dtype=np.uint8)
    i[:,:,color] = img
    c = [0,0,0]
    c[color] = 255

    i = cv2.putText(i, str(obs_id), (2,12), cv2.FONT_HERSHEY_PLAIN, fontScale=1.0, color=c)
    return i


all_rows = [r for r in rows]
n, m = all_rows[0]['image'].shape


irr = 0
while True:
    irrh = min(len(all_rows), irr + n_imgs_max)
    srr = slice(irr, irrh)
    print('srr', srr)
    rows = all_rows[srr]
    
    img_name = f'IMAGE BATCH: {srr} / {len(all_rows)}'


    n_rows = len(rows) / n_cols
    if n_rows % 1 > 0:
        n_rows = int(np.ceil(n_rows))
    else:
        n_rows = int(n_rows)

    print('n_rows', n_rows, len(rows) / n_cols)
    full_img = np.zeros((n*n_rows, m*n_cols,3), dtype=np.uint8)
    lut = np.full((n*n_rows, m*n_cols), -1, dtype=np.uint16)
    slices = []

    i = 0
    for ir in range(n_rows):
        for ic in range(n_cols):
            if i >= len(rows):
                break

            sx = slice(ir*m, (ir+1)*m)
            sy = slice(ic*m, (ic+1)*m)
            
            slices.append((sx, sy))

            full_img[sx, sy] = make_color(rows[i])
            lut[sx, sy] = i

            i += 1

    
    cv2.namedWindow('image')
    cv2.setMouseCallback('image',toggle_bad_good)

    # print(full_img)
    while(1):
        cv2.imshow('image',full_img)
        cv2.setWindowTitle('image', img_name )

        k = cv2.waitKeyEx(100)
        command = ''
        if k == 27:
            command = 'quit'
        elif k == ord('q'):
            command = 'quit'
        elif k == ord('s'):
            command = 'save'

        elif k in [54, 2555904, ord('n'), ord('+')]:
            command = 'next'
        elif k in [52, 2424832, ord('l'), ord('-')]:
            command = 'last'
        
        if k != -1:
            print(k, command)

            if command == 'save':
                print('saving...')
                update_rows(rows)
                print('done')
            elif command == 'quit' or command == 'next' or command == 'last':
                break
                    

    if command == 'quit':
        break
    elif command == 'next':
        print('saving...')
        update_rows(rows)
        print('done')
        irr += n_imgs_max
    elif command == 'last':
        print('saving...')
        update_rows(rows)
        print('done')
        irr -= n_imgs_max
    
    if irr < 0:
        irr = 0
        while irr + n_imgs_max < len(all_rows):
            irr += n_imgs_max
    elif irr > len(all_rows):
        irr = 0

