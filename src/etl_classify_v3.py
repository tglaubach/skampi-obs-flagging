#!/usr/bin/env python
# coding: utf-8


import numpy as np
import os
import io
from PIL import Image
import json
import datetime
import sqlite3
import datetime
import time
import traceback
import gzip

import pandas as pd

import astropy.units as u
from astropy.time import Time
from astropy.coordinates import EarthLocation

import pp_otf_img, pp_extract, pp_helpers, pp_obs_clf_nn, pp_sqlite


pp_extract.init_db()


#############################################################################################
#############################################################################################
# 
# ██████   █████  ██████   █████  ███    ███ ███████ 
# ██   ██ ██   ██ ██   ██ ██   ██ ████  ████ ██      
# ██████  ███████ ██████  ███████ ██ ████ ██ ███████ 
# ██      ██   ██ ██   ██ ██   ██ ██  ██  ██      ██ 
# ██      ██   ██ ██   ██ ██   ██ ██      ██ ███████ 
# 
#############################################################################################
#############################################################################################                                        
                                                   

receiver = 'Ku'
scan_type = 'otf'

lat = -30.7249
lon = 21.45714
altitude = 1086

classifier = 'models/20231019_cnn_model_otf_ku_*.keras'
sqlite_file = 'data/tagging_db_v3.sqlite'

dt_wait = 30 * 60 # time to wait between checking for new files in seconds

ids_handled = []
# ids_handled += list(range(22992)) #  too old

ids = None

#############################################################################################
#############################################################################################
# 
# ██   ██ ███████ ██      ██████  ███████ ██████  ███████ ███████ 
# ██   ██ ██      ██      ██   ██ ██      ██   ██ ██      ██      
# ███████ █████   ██      ██████  █████   ██████  █████   ███████ 
# ██   ██ ██      ██      ██      ██      ██   ██ ██           ██ 
# ██   ██ ███████ ███████ ██      ███████ ██   ██ ███████ ███████                                                              
# 
#############################################################################################
#############################################################################################

loc = EarthLocation(lat=lat*u.deg, lon=lon*u.deg, height=altitude*u.m)  
mdl = pp_obs_clf_nn.TaggingClassifier.load_model(classifier)


#############################################################################################
#############################################################################################
#
# ███████ ████████ ██          ███████ ██    ██ ███    ██ 
# ██         ██    ██          ██      ██    ██ ████   ██ 
# █████      ██    ██          █████   ██    ██ ██ ██  ██ 
# ██         ██    ██          ██      ██    ██ ██  ██ ██ 
# ███████    ██    ███████     ██       ██████  ██   ████ 
# 
#############################################################################################
#############################################################################################


#############################################################################################
#############################################################################################

def fun_test1(dc):
    assert not np.any([v is None for v in dc.values()]), '   db row entry has None --> skipping'
    assert os.path.exists(fname), '   file does not exists --> skipping'
    assert obs_id not in dc['ids_handled'], '   file already handled --> skipping'
    return dc

#############################################################################################
#############################################################################################

def fun_pp_extr2(dc):
    print('   loading hdf5...')
    ff = dc['fname']
    dc['data'] = pp_extract.load_hdf5(ff, full=False, weather=True)
    return dc

#############################################################################################
#############################################################################################

def fun_pp_trans1(dc):
    print(' '*6, 'image processing...')
    ff = dc['fname']
    dc['data'] = pp_otf_img.run_single(dc['data'], ff, do_plot_img_p=False, steps_todo=['grd_nn', 'bgfit'], pxls=(100,100))
    return dc

#############################################################################################
#############################################################################################

def fun_pp_trans2(dc):
    print(' '*6, 'weather...')
    weather = {}
    for k, v in dc['data']['weather'].items():
        if np.all(np.isnan(v)) or len(v) == 0:
            if k == 'wind_speed_gusts' \
                and 'wind_speed' in dc['data']['weather']\
                and not np.all(np.isnan(dc['data']['weather']['wind_speed']))\
                and not len(dc['data']['weather']['wind_speed']) == 0:
                weather[k] = np.nanmax(dc['data']['weather']['wind_speed'])
            else:
                weather[k] = np.nan
        else:
            weather[k] = np.nanmean(v)

    weather['obs_id'] = dc['obs_id']
    # print((' '*6) + str(weather))

    dc['tabledata']['weather'] = weather
    return dc

#############################################################################################
#############################################################################################

def fun_pp_trans3(dc):
    print(' '*6, 'tagging...')
    image = dc['data']['Z']

    assert not np.any(np.isnan(image)), '   found NaN in image data --> skipping'

    tags = mdl.predict(image)

    print(' '*6 + 'classification result: ' + str(tags))

    tags = ' '.join([str(t) for t in tags])

    image -= np.min(image)
    image /= np.max(image)
    image *= 255

    im = Image.fromarray(image.astype(np.uint8))

    dc['tabledata']['tagging'] = {
        'obs_id': dc['obs_id'],
        'filename': dc['fname'],
        'rawdata': im,
        
        'ra': gzip.compress(dc['data']['ra'].tobytes()),
        'dec': gzip.compress(dc['data']['dec'].tobytes()),
        'pwr': gzip.compress(dc['data']['pwr'].tobytes()),
        'tags': tags,
        
        'classifier': mdl.classifier_name, 
        'clf_chksum':  mdl.clf_chksum, 
    }

    return dc

#############################################################################################
#############################################################################################

def fun_pp_trans4(dc):
    print(' '*6, 'pointing...')
    row = dc['tabledata']['row']
    def mapper(dc):
        return {
            'obs_id': dc['obs_id'],
            'src_ra': dc['src_ra'],
            'src_dec': dc['src_dec'],
            'pkfit_ra': dc['fit_x0'],
            'pkfit_dec': dc['fit_y0'],
            
            'pk_time': dc['pk_time'],

            'pk_az': dc['pk_az'],
            'pk_el': dc['pk_el'],

            
            'c_az': dc['pk_az_appl_corr'],
            'c_el': dc['pk_el_appl_corr'],
        }
    
    tmp = mapper(row)
    assert not any([True for v in tmp.values() if v is None or np.isnan(v)]), '   mapped column for pointing calculation contains None or nan --> skipping'
        
    df_point = pd.DataFrame([tmp]).set_index('obs_id')
    df_point = pp_helpers.calc_and_add_offsts(df_point, loc)
    dc['tabledata']['pointing'] = df_point.reset_index(drop=False).to_dict('records')
    return dc

#############################################################################################
#############################################################################################



def fun_pp_upload(dc):
    print('   saving...')
    for k, v in dc['tabledata'].items():
        if k == 'tagging':
            pp_sqlite.write_obs_tagging(sqlite_file, [v], set_info_auto=True)
        else:
            if not isinstance(v, list):
                v = [v]
                
            pp_sqlite.write_rows_pandas(sqlite_file, v, k)


#############################################################################################
#############################################################################################

# ███    ███  █████  ██ ███    ██     ██       ██████   ██████  ██████  
# ████  ████ ██   ██ ██ ████   ██     ██      ██    ██ ██    ██ ██   ██ 
# ██ ████ ██ ███████ ██ ██ ██  ██     ██      ██    ██ ██    ██ ██████  
# ██  ██  ██ ██   ██ ██ ██  ██ ██     ██      ██    ██ ██    ██ ██      
# ██      ██ ██   ██ ██ ██   ████     ███████  ██████   ██████  ██      
                                                                      
#############################################################################################
#############################################################################################


while 1:

    ids_handled += pp_sqlite.get_ids_done(sqlite_file)
    ids_handled = list(set(ids_handled))
    
    is_started = False

    print(Time.now().iso, ' | STARTING! ')
    
    gen = pp_extract.query_ska_proto_db(receiver, scan_type, ids = ids, manual_exclude_list=ids_handled)
    n_rows = next(gen)
    
    print('#'*50)
    print('RUNNING NEW QUERY WITH N={} ROWS'.format(n_rows))
    print('#'*50)
    
    for ii, dc_row in enumerate(gen):
        try:
            dc = dict(ids_handled=ids_handled, tabledata={})
            dc['tabledata']['row'] = dc_row
            dc['obs_id'] = dc['tabledata']['row']['obs_id']
            dc['fname'] = dc['tabledata']['row']['fname']
            if not os.path.exists(dc['fname'] ):
                ids_handled += [obs_id]
                continue
                
            obs_id = dc['obs_id']
            fname = dc['fname']
            
            if is_started:
                print('-'*50)
                print(Time.now().iso, ' | ITER={}/{} | obs_id={} | {}'.format(ii, n_rows, obs_id, fname))

            dc = fun_test1(dc)
            is_started=True

            dc = fun_pp_extr2(dc)

            dc = fun_pp_trans1(dc)
            dc = fun_pp_trans2(dc)
            dc = fun_pp_trans3(dc)
            dc = fun_pp_trans4(dc)

        except AssertionError as err:
            print(err)
            ids_handled += [obs_id]
            continue
            
        except Exception as err:
            print('ERROR!')
            print(traceback.print_exc())
            #raise
            print(err)
            print('   --> SKIPPING!')
            continue

        fun_pp_upload(dc)

        ids_handled += [obs_id]
        print('   done with tick!')
        
        if ii > 0 and ii % 200 == 0:
            print(Time.now().iso, '| CLEANING UP...')
            conn = sqlite3.connect(sqlite_file)
            conn.execute("VACUUM")
            conn.close()




    print(Time.now().iso, '| CLEANING UP...')

    conn = sqlite3.connect(sqlite_file)
    conn.execute("VACUUM")
    conn.close()
    
    td = datetime.timedelta(seconds=dt_wait)
    s = '{} | HANDLED ALL FILES. WILL CHECK FOR NEW FILES IN {} ... '.format(Time.now().iso, td)
    print(s)
    time.sleep(dt_wait)

