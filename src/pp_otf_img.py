import numpy as np

from scipy.interpolate import Rbf
from scipy.spatial import KDTree
from scipy import ndimage

import matplotlib.pyplot as plt

import pp_extract

import re

def __interp_2d_nn(ra, dec, pwr, ra_i, dec_i):
    
    A = np.vstack([ra, dec]).T
    A1 = np.vstack([ra_i, dec_i]).T
    
    kdtree = KDTree(A)
    d, idxs = kdtree.query(A1)
    return pwr[idxs]

def __lin_fit(X, Y, Z):
    Xv = np.array([X, Y]).T
    Xv = np.hstack((np.ones((X.size, 1)), Xv))
    XXv = np.dot(Xv.transpose(), Xv)
    XXXv = np.dot(np.linalg.pinv(XXv), Xv.transpose())
    theta = np.dot(XXXv, Z)

    return (theta[0] + X * theta[1] + Y * theta[2])

def __detrend_X(X, Y, Z, k = 0.1):
    n, m = Z.shape
    idx = np.ones_like(Z[0,:], dtype=bool)
    mmm = int(k * m)
    idx[mmm:-mmm] = False
    
    row_means = np.mean(Z[:, idx], axis=1)
    tmp = np.tile(row_means, (m, 1)).T
    
    return Z - tmp
    
    
def __detrend_X1(X, Y, Z, k = 0.1):
    n, m = Z.shape
    idx = np.ones_like(Z[0,:], dtype=bool)
    mm = int(k * m)

    row_means1 = np.mean(Z[:, :mm], axis=1)
    row_means2 = np.mean(Z[:, -mm:], axis=1)

    dy = row_means2 - row_means1
    dx = m
    x = np.arange(0, m)

    slope = np.tile(dy/dx, (m, 1)).T
    b = np.tile(row_means1, (m, 1)).T

    mdl = x * slope + b

    return Z - mdl
    
def grd_rbf(ZI, ra, dec, pwr, XI, YI, ret_stps, args):
    # ============================
    # RBF interpolation
    # ============================
    temp = re.findall(r'\d+', args)
    eps = list(map(int, temp))[0]
    rbf = Rbf(ra, dec, pwr, functionstr='gaussian', epsilon=eps)        
    ZI = rbf(XI, YI)
    if ret_stps:
        stp = (XI.flatten(), YI.flatten(), ZI.flatten(), 'RBF interp 2 grid', None)
    else:
        stp = None

    return ZI, stp


def grd_nn(ZI, ra, dec, pwr, XI, YI, ret_stps, args):

    # ============================
    # NN interpolation
    # ============================
    ZI = __interp_2d_nn(ra, dec, pwr, XI.flatten(), YI.flatten())
    ZI = np.reshape(ZI, XI.shape)

    if ret_stps:
        stp = (XI.flatten(), YI.flatten(), ZI.flatten(), 'NN interp 2 grid', None)
    else:
        stp = None

    return ZI, stp


def grd_nn(ZI, ra, dec, pwr, XI, YI, ret_stps, args):

    # ============================
    # NN interpolation
    # ============================
    ZI = __interp_2d_nn(ra, dec, pwr, XI.flatten(), YI.flatten())
    ZI = np.reshape(ZI, XI.shape)

    if ret_stps:
        stp = (XI.flatten(), YI.flatten(), ZI.flatten(), 'NN interp 2 grid', None)
    else:
        stp = None

    return ZI, stp

def med(ZI, ra, dec, pwr, XI, YI, ret_stps, args):
    
    # using re.findall()
    # getting numbers from string 
    temp = re.findall(r'\d+', args)
    size = list(map(int, temp))[0]

    # ============================
    # Median Filter 
    # ============================
    ZI = ndimage.median_filter(ZI, size=size)

    if ret_stps:
        stp = (XI.flatten(), YI.flatten(), ZI.flatten(), 'median filter', None)
    else:
        stp = None
    
    return ZI, stp

def gauss(ZI, ra, dec, pwr, XI, YI, ret_stps, args):
    
    # using re.findall()
    # getting numbers from string 
    temp = re.findall(r'\d+', args)
    sigm = list(map(int, temp))[0]

    # ============================
    # Gaussian Filter 
    # ============================
    ZI = ndimage.gaussian_filter(ZI, sigma=sigm)

    if ret_stps:
        stp = (XI.flatten(), YI.flatten(), ZI.flatten(), 'gauss filter', None)
    else:
        stp = None
    
    return ZI, stp

def bgfit(ZI, ra, dec, pwr, XI, YI, ret_stps, args):
    # ============================
    # Substract Background
    # ============================

    bg = __lin_fit(XI.flatten(), YI.flatten(), ZI.flatten())
    BG = np.reshape(bg, XI.shape)
    ZI = ZI - BG

    lims = (np.min(ZI.flatten()), np.max(ZI.flatten()))
    if ret_stps:
        stp = (XI.flatten(), YI.flatten(), ZI.flatten(), 'subs. background', lims)
    else:
        stp = None

    return ZI, stp



def rfiline(ZI, ra, dec, pwr, XI, YI, ret_stps, args):
    # ============================
    # Substract line Noise
    # ============================

    ZI = __detrend_X1(XI, YI, ZI)

    lims = (np.min(ZI.flatten()), np.max(ZI.flatten()))
    if ret_stps:
        stp = (XI.flatten(), YI.flatten(), ZI.flatten(), 'subs. line Noise', lims)
    else:
        stp = None
        
    return ZI, stp



def im_p(ra, dec, pwr, ret_stps=False, steps_todo=['grd_nn', 'med|17', 'bgfit', 'rfiline', 'med|17'], pxls=(101, 101), lims_x=(0.15, 0.85), lims_y=(0.0, 1.0), p_crop=0.2):

    steps = []
    old_lst = ['grd_nn', 'med17', 'bgfit', 'rfiline', 'med17']
    proc_steps = []
    for el in steps_todo:
        if isinstance(el, int):
            e = old_lst[el]
        else:
            e = el
        
        proc_steps.append(e)

    if len(proc_steps) == 0 or (not proc_steps[0].startswith('grd')):
        print('WARNING: No gridding supplied. will automatically add NN gridding')
        proc_steps = [old_lst[0]] + proc_steps
 
        
    nn = int(len(ra) * p_crop)      
    tmp = np.ptp(ra[nn:-nn])
    tmp2 = np.ptp(dec[nn:-nn])

    if tmp > 180.0: tmp -= 360

    d_ra_min, d_ra_max = np.min(ra[nn:-nn]) + np.array(lims_x) * tmp
    d_dec_min, d_dec_max = np.min(dec[nn:-nn]) + np.array(lims_y) * tmp2

    ra_i = np.linspace(d_ra_min, d_ra_max, pxls[0])
    dec_i = np.linspace(d_dec_min, d_dec_max, pxls[1])

    XI, YI = np.meshgrid(ra_i, dec_i)
    
    # ============================
    # INITIAL TIME DATA 
    # ============================

    if ret_stps:
        stp = (ra, dec, pwr, 'initial', None)
        steps.append(stp)


    dc_funcs = {
        'grd_rbf': grd_rbf,
        'grd_nn': grd_nn,
        'med': med, 
        'gauss': gauss,
        'bgfit': bgfit,
        'rfiline': rfiline
    }

    ZI = None
    print('image processing | running the following steps: {}'.format(proc_steps))

    for s in proc_steps:
        if '|' in s:
            (name, args) = s.split('|')
        else:
            name, args = s, None

        fun = dc_funcs[name]
        ZI, stp = fun(ZI, ra, dec, pwr, XI, YI, ret_stps, args)
        steps.append(stp)


    # ============================
    # END
    # ============================
    if ret_stps:
        return XI, YI, ZI, steps
    else:
        return XI, YI, ZI


def plt_img_p_steps(steps, ttl = ''):

    (ra, dec, _, _, _) = steps[0]

    (x, y, z, ttl, lim) = steps[2]
    pwr_min, pwr_max = np.min(z), np.max(z)
    (x_min, x_max) = np.min(x) - 0.1, np.max(x) + 0.1
    (y_min, y_max) = np.min(y) - 0.1, np.max(y) + 0.1


    f, axes = plt.subplots(1,len(steps), figsize=(20, 2))
    axes = axes.flatten()

    plt.suptitle(ttl)

    for ax, stp in zip(axes, steps):
        (x, y, z, ttl, lim) = stp

        l = (pwr_min, pwr_max) if lim is None else lim

        cm = ax.scatter(x, y, c=z) #, vmin=l[0], vmax=l[1])
        ax.plot(ra, dec, '-k', linewidth=0.2)
        ax.set_xlabel('RA [deg]')
        ax.set_ylabel('DEC [deg]')
        ax.axis('equal')
        ax.set_title(ttl)
        ax.set(xlim=(x_min, x_max), ylim=(y_min, y_max))
        plt.subplots_adjust(top=0.75)
        if ax != axes[0]:
            ax.set_yticklabels([])

    plt.colorbar(cm, label='power [ ]', ax=ax)


def plot_imgs(imgs, df, idx_red=[], n_rows_max=6, n_cols=6):
    n = len(imgs)
    for cnt, (obs_id, ZI) in enumerate(imgs.items()):

        if cnt % (n_rows_max*n_cols) == 0:
            n_rows = int(min(np.ceil(n - cnt / n_rows_max), n_cols))
            n_rows = max(n_rows, 1)
            
            f, axes = plt.subplots(n_rows, n_cols, figsize=(3*n_cols,3*n_rows))
            axes = axes.flatten()

        oldrow = df.loc[obs_id, :]
        ff = '/data/output/' + df.loc[obs_id, 'fname']

        print('{:4d}/{} ({:.1f}%) | {}'.format(cnt, n, cnt/n * 100, ff), end='\r')

        i = cnt % (n_rows*n_cols)
        ax = axes[i]

        ax.imshow(ZI)

        color = 'r' if obs_id in idx_red else 'k'

        ax.set_title('obs_id: {:.0f} | src_id: {:.0f}'.format(obs_id, oldrow['src_id']), color=color)
        ax.set_xticklabels([])
        ax.set_yticklabels([])


    if i < len(axes):
        for ax in axes[i+1:]:
            ax.remove()



def run(lst, do_plot_img_p=False, full_load = True, steps_todo=[1, 2, 3, 4], pxls=(100, 100), skip_faulty=True):
    

    n = len(lst)
    powers = {}
    n_plot = -1
    
    if isinstance(do_plot_img_p, bool):
        n_plot = len(powers) if do_plot_img_p else -1
    elif isinstance(do_plot_img_p, int):
        n_plot = do_plot_img_p

    for cnt, ff in enumerate(lst):
        try:
            s = '{:4d}/{} ({:.1f}%) | {}'.format(cnt, n, cnt/n * 100, ff)

            print(s)

            dc = pp_extract.load_hdf5(ff, full=full_load)

            ra, dec, pwr = dc['ra'], dc['dec'], dc['pwr']

            # ___________________________

            if cnt < n_plot:
                XI, YI, ZI, steps = im_p(ra, dec, pwr, ret_stps=True, steps_todo=steps_todo, pxls=pxls)
                plt_img_p_steps(steps, ff)
            else:
                XI, YI, ZI = im_p(ra, dec, pwr, ret_stps=False, steps_todo=steps_todo, pxls=pxls)

            dc['X'] = XI
            dc['Y'] = YI
            dc['Z'] = ZI
            # ___________________________

            powers[ff] = dc

            # ___________________________
        except Exception as err:
            if skip_faulty:
                print(f'ERROR: {err}')
                print('SKIPPING!')
            else:
                raise
                
    return powers
            
def run_single(dc, ff='', do_plot_img_p=False, steps_todo=[1, 2, 3, 4], pxls=(100, 100)):

    ra, dec, pwr = dc['ra'], dc['dec'], dc['pwr']

    # ___________________________

    if do_plot_img_p:
        XI, YI, ZI, steps = im_p(ra, dec, pwr, ret_stps=True, steps_todo=steps_todo, pxls=pxls)
        plt_img_p_steps(steps, ff)
    else:
        XI, YI, ZI = im_p(ra, dec, pwr, ret_stps=False, steps_todo=steps_todo, pxls=pxls)

    dc['X'] = XI
    dc['Y'] = YI
    dc['Z'] = ZI
    # ___________________________

    return dc


