import numpy as np

import matplotlib.pyplot as plt

from scipy.optimize import curve_fit

import pandas as pd

import pp_extract


def gauss2d(XY, A, x0, y0, sx, sy, b, theta=None):
    """
    2D gauss
    """

    if theta is None:
        return A * np.exp(-.5 * ((XY[0] - x0)**2 / sx**2 + (XY[1] - y0)**2 / sy**2)) + b
    else:
        k1 = +((np.cos(theta)**2) / (2*sx**2)) + ((np.sin(theta)**2) / (2*sy**2))
        k2 = -((np.sin(2*theta)) / (4*sx**2)) + ((np.sin(2*theta))  / (4*sy**2))
        k3 = +((np.sin(theta)**2) / (2*sx**2)) + ((np.cos(theta)**2) / (2*sy**2))
        return A * np.exp(-(k1*(XY[0] - x0)**2 + 2*k2*(XY[0] - x0) * (XY[1] - y0) + k3 * (XY[1] - y0)**2)) + b


def plot_gauss_contour(sub, fitResult, nsigma=2, c='r', m='*'):
    """
    Adds a contour for a 2D gauss from fitResult to plot
    """
    x = np.linspace(*sub.get_xlim(), num=100)
    y = np.linspace(*sub.get_ylim(), num=100)
    A = gauss2d(np.meshgrid(x, y), *fitResult)

    levels = 10 * np.log10(np.asarray([fitResult[0] * np.exp(-0.5 * nsigma**2), fitResult[0]]))

    sub.plot([fitResult[1]], [fitResult[2]], c=c, marker=m, label='Observed', ls='None')
    sub.contour(x, y, 10*np.log10(A), levels=levels, colors=c)



def fit2D_Gauss_simple(X, Y, power, beam_width=.25, allow_rotate=True):
    
    XY_bounds = [[X.min(), X.max()], [Y.min(), Y.max()]]
    
    correctedPower = power
    
    noise_level = np.std(correctedPower)
    
    theta = 0.0 if allow_rotate else None
    # Start with maximum power
    idx = np.argmax(correctedPower)
    p0 = [correctedPower[idx], X[idx], Y[idx], beam_width/2, beam_width/2, 0.0]

    bounds = [(noise_level, correctedPower.max() * 1.5),
              XY_bounds[0], XY_bounds[1],               
               (beam_width/2 *0.75, beam_width/2 * 1.25),
               (beam_width/2 *0.75, beam_width/2 * 1.25),
               (-np.inf, np.inf)]
    
    if allow_rotate:
        bounds.append((0.0, np.pi))
        p0.append(0.0)

    bnds = [tuple([b[0] for b in bounds]), tuple([b[1] for b in bounds])]

    fitResult, fitCov = curve_fit(gauss2d, (X, Y), correctedPower, tuple(p0), bounds=bnds)

    # estimate goodness-of-fit. All data points have the same error
    model = gauss2d((X, Y), *fitResult)
    gof = np.sum((power - model )**2) / power.size

    bestGOF = gof
    bestFitResult = fitResult
    bestFitCov = fitCov
    
    
    result = dict(
           method="2DGauss_vSimple",
           peak=dict(
                amplitude=bestFitResult[0],
                background=0.0,
               ),
           peak_fit=dict(
                parameter=bestFitResult.tolist(),
                covariance=bestFitCov.tolist(),
                gof=bestGOF
               ),
           background_fit=dict(
                parameter=[],
                covariance=None,
                gof=None,
               ),
           position_bounds=dict(ra=bounds[1], dec=bounds[2]) 
            )

    return result




def run(powers, df, dish_location, do_plot_fit_new=False):

    n_plot = -1
    if isinstance(do_plot_fit_new, bool):
        n_plot = len(powers) if do_plot_fit_new else -1
    elif isinstance(do_plot_fit_new, int):
        n_plot = do_plot_fit_new

    rows = {}
    for cnt, (i, dc) in enumerate(powers.items()):

        # ========================
        # PREPARATION
        # ========================
        cols = 't, ra, dec, pwr, X, Y, Z, beam_width'.split(', ')

        (t, ra, dec, pwr, XI, YI, ZI, beam_width) = [dc[c] for c in cols]

        oldrow = df.loc[i, :]
        
        ff = '/data/output/' + df.loc[i, 'fname']
        oldfit = list(oldrow.loc[['fit_A', 'fit_x0', 'fit_y0', 'fit_sx', 'fit_sy', 'fit_b']])
        

        print('{:4d}/{} ({:.1f}%) | {} | BW: {:.4f}'.format(cnt, len(powers), cnt/len(powers) * 100, ff, beam_width), end='\r')

        # rescale
        ZI = ZI - np.min(ZI)
        ZI /= np.mean(ZI)

        # make 1D
        x, y, z = XI.flatten(), YI.flatten(), ZI.flatten()

        # get maxima position
        idx = np.argmax(z)
        xm, ym = x[idx], y[idx]

        # ========================
        # FITTING
        # ========================
        fitResult = fit2D_Gauss_simple(x, y, z, beam_width=beam_width)
    
        peak_par = fitResult['peak_fit']['parameter']

        model = gauss2d((x, y), *peak_par)
        resid = z - model

        
        # some KPIs
        gof = fitResult['peak_fit']['gof']
        in_sum = np.sum(z)
        resid_sum = np.sum(np.abs(resid))
        resid_std = np.std(resid)

        (A, x0, y0, sx, s1, b, theta) = peak_par
            
        
        # distance from image maxima
        dx, dy = xm - x0, ym - y0    
        dxy = np.sqrt(dx ** 2 + dy **2 )

        # nearest scan index
        ii_nn = np.argmin(np.sqrt((ra-x0) ** 2 + (dec-y0) **2 ))
        t0 = t[ii_nn]
#         t0 = np.mean(t)
        
        if cnt < n_plot:
            # ========================
            # PLOTTING
            # ========================

            f, (ax1, ax2, ax3) = plt.subplots(1,3, figsize=(12,2))

            ax = ax1
            cm = ax.scatter(ra, dec, c=pwr)
            plt.colorbar(cm, label='power [ ]', ax=ax)
            ax.plot(ra, dec, '-k', linewidth=0.2)
            ax.set_xlabel('RA [deg]')
            ax.set_ylabel('DEC [deg]')
            ax.axis('equal')
            ax.set_title('RAW DATA')

            ax=ax2

            cm = ax.scatter(x,y,c=z)
            plt.colorbar(cm, label='power [ ]', ax=ax)

            ax.set_xlabel('RA [deg]')
            ax.axis('equal')
            ax.set_title('FIT DATA | sum: {:.2e}'.format(np.sum(z)))
            ax.set_yticklabels([])

            ax.plot(xm, ym, ' +r', markersize=15)
            ax.plot(ra[ii_nn], dec[ii_nn], ' xk', markersize=15)
            plot_gauss_contour(ax, peak_par)

            plot_gauss_contour(ax, oldfit, c='m')

            ax=ax1
            plot_gauss_contour(ax, peak_par)
            plot_gauss_contour(ax, oldfit, c='m')

            ax = ax3
            cm = ax.scatter(x,y,c=resid)
            plt.colorbar(cm, label='residual [ ]', ax=ax)

            ax.set_xlabel('RA [deg]')
            ax.axis('equal')
            ax.set_title('RESIDUAL | GOF: {:.3f}'.format(fitResult['peak_fit']['gof']))
            ax.set_yticklabels([])


            plt.subplots_adjust(top=0.75)
            plt.suptitle('obs_id: {:.0f} | src_id: {:.0f} | {}'.format(i, oldrow['src_id'], ff))

            # ========================
            # WRITE RESULTS
            # ========================


        rows[i] = [gof, in_sum, resid_sum, resid_std, dx, dy, dxy, A, t0, x0, y0, sx, s1, b, theta]
    
    cols = 'gof, in_sum, resid_sum, resid_std, d_ra_0m, d_dec_0m, d_rms_0m, fit_A, fit_t0, fit_ra0, fit_dec0, fit_sx, fit_s1, fit_b, fit_theta'.split(', ')   
    
    df_fits = pd.DataFrame(rows.values(), index=rows.keys(), columns=cols)
    df_fits = pp_extract.recalc_on_sky(df_fits, df, powers, dish_location)

    return df_fits

     

    