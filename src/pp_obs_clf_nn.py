
import os
import PIL
import io
import glob
import re
import hashlib


import numpy as np
import keras


class TaggingClassifier():
    

    def load_model(model_ensemble_name, thresholds=0.25, needs_rescaling = False):
        if not isinstance(model_ensemble_name, list):
            files = glob.glob(model_ensemble_name)
        else:
            files = model_ensemble_name
        
        classes = []
        inputs = []
        outputs = []
        
        hash_obj = hashlib.md5()    
        
        for file in files:
            tagname = file.split('_tag_')[-1].split('.')[0]
            print(file, '-->', tagname)
            assert tagname, 'tagname can not be empty!'
            
            hash_obj.update(open(file, 'rb').read())
                
            mdl = keras.models.load_model(file)
            
            classes.append(tagname)
            inputs.append(mdl.input)
            outputs.append(mdl.output)
            
        clf_chksum = hash_obj.hexdigest()

        model = keras.Model(
            inputs=inputs,
            outputs=outputs
        )
        clf = TaggingClassifier(model, clf_chksum, 
                                classifier_name = model_ensemble_name,     
                                classes = classes,
                                thresholds=thresholds,                                      
                                needs_rescaling=needs_rescaling,      
                                filenames=list(files))
        return clf
    

    def __init__(self, model:dict, clf_chksum:str, classifier_name:str='', classes=None, thresholds=0.25, needs_rescaling = True, filenames=None) -> None:
        if not classes:
            classes = [inp.name for inp in model.output]
            
            
        if np.isscalar(thresholds):
            thresholds = {k:thresholds for k in classes}
        elif isinstance(thresholds, dict):
            pass
        elif isinstance(thresholds, list) and len(thresholds) == len(classes):
            thresholds = dict(zip(classes, thresholds))
        else:
            raise ValueError('thresthold must be of type dict with the same keys as model, or a scalar for all classifiers')
        

        self.model = model
        self.classes = classes
        self.thresholds = thresholds
        self.needs_rescaling = needs_rescaling
        self.filenames = filenames
        self.classifier_name = classifier_name
        self.clf_chksum = clf_chksum
        
    def __call__(self, image):
        return self.predict(image)
    
    @property
    def tags(self):
        return self.classes
    
    def summary(self):
        return self.model.summary()
    
    def plot(self, filename=None):
        if not filename:
            filename = self.classifier_name.replace('*', '').split('.')[0] + '.png'
        print(filename)
        keras.utils.plot_model(self.model, filename, show_shapes=True)
        
    def predict(self, image, verbose=0):

        # input handling
        if isinstance(input, dict):
            image = input['image']
        if isinstance(self, list):
            return [self.predict(i) for i in input]
        if isinstance(image, bytes):
            image = np.array(PIL.Image.open(io.BytesIO(image), dtype=np.uint8))
        if len(image.shape) > 2 and image.shape[-1] != 1:
            return np.array([self.predict(image[i,:,:]) for i in range(image.shape[0])])
        
        n, m = image.shape
        x = image.reshape(1, n, m, 1)

        x = x.astype(float)
        if self.needs_rescaling:
            x -= np.min(x)
            x /= np.max(x)
        
        inp = [x] * len(self.model.input)
        preds = self.model.predict(inp, verbose=min(0, verbose-1))
        preds_dc = {k:pred[0,0] for k, pred in zip(self.classes, preds)}
        predictions = [k for k,v in preds_dc.items() if v > self.thresholds[k]]

        if verbose:
            print('   --> classification: {} (scr: {})'.format(predictions, preds_dc))
            
        return predictions



if __name__ == "__main__":
    
    pth = os.path.abspath('models/20231019_cnn_model_otf_ku*.keras')
    print(pth)
    mdl = TaggingClassifier.load_model(pth)

    print(mdl.filenames)
    print(mdl.summary())
    
    size = 100
    fwhm = 15
    x = np.arange(0, size, 1, float)
    y = x[:,np.newaxis]
    x0 = y0 = size // 2
    x = np.exp(-4*np.log(2) * ((x-x0)**2 + (y-y0)**2) / fwhm**2)
    
    print('SHAPE', x.shape)
    mdl.plot()
    print(mdl.predict(x, verbose=1))