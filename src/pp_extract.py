
import numpy as np
from astropy.coordinates import SkyCoord
import astropy.units as u
from astropy.time import Time

import h5py

from scipy import interpolate

import sqlalchemy
from ska_proto.MSAproto.core import schema
from ska_proto.MSAproto.core.tags import TAGS

def init_db():
    schema.init_db()

def query_ska_proto_db(receiver, scan_type, ids=None, sources_excl_list=None, manual_exclude_list=None, filters=[], additional_cols={}):

    # assert 'schema' in locals() and 'TAGS' in locals(), 'could nor find schema and tags modules. Did you forgot to call init_skaproto() first?'

#     schema = locals()['schema']
#     TAGS = locals()['TAGS']
    
    rec_dc = {
        'ku': 2,
        's': 3
    }

    scan_dc = {
        'otf': 2,
        'az': 3,
        'el': 4
    }

    rec = rec_dc[receiver.lower()]
    scn = scan_dc[scan_type.lower()]



    # # Query the Database and extract Data


    cols = {
        'obs_id': schema.PointSourceFit.observation_id,
        'scn_no': schema.Scan.scannumber,
        'scn_time_unx': schema.Scan.starttime,

        'src_ra': schema.Source.ra,
        'src_dec': schema.Source.dec,
        'src_id': schema.Source.id,
        'src_flux': schema.Source.flux,

        'pk_amp': schema.PointSourceFit.data['peak']['amplitude'],
        'pk_az': schema.PointSourceFit.data['peak']['position']['az'],
        'pk_el': schema.PointSourceFit.data['peak']['position']['el'],
        'pk_az_appl_corr': schema.PointSourceFit.data['peak']['applied_correction']['az'],
        'pk_el_appl_corr': schema.PointSourceFit.data['peak']['applied_correction']['el'],
        'pk_time': schema.PointSourceFit.data['peak']['time'],
        'pk_bckgrnd': schema.PointSourceFit.data['peak']['background'],
        
        'pk_bck_p0': schema.PointSourceFit.data['background_fit']['parameter'][0],
        'pk_bck_dx': schema.PointSourceFit.data['background_fit']['parameter'][1],
        'pk_bck_dy': schema.PointSourceFit.data['background_fit']['parameter'][2],
        
        'fit_A': schema.PointSourceFit.data['peak_fit']['parameter'][0],
        'fit_x0': schema.PointSourceFit.data['peak_fit']['parameter'][1],
        'fit_y0': schema.PointSourceFit.data['peak_fit']['parameter'][2],
        'fit_sx': schema.PointSourceFit.data['peak_fit']['parameter'][3],
        'fit_sy': schema.PointSourceFit.data['peak_fit']['parameter'][4],
        'fit_b': schema.PointSourceFit.data['peak_fit']['parameter'][5],
        
        'fit_gof': schema.PointSourceFit.data['peak_fit']['gof'],
        
        'obs_epoch': schema.Observation.observation_epoch,
        'fname': schema.Observation.filename,
    }

    if additional_cols:
        cols = {**cols, **additional_cols}

    with schema.get_session() as session:

        #queries for filters; we dont want the "bad" observations with an error tag
        bad_observations = session.query(schema.ObservationTag.observation_id).filter(sqlalchemy.or_(schema.ObservationTag.type <0, schema.ObservationTag.type==TAGS.SOFTWARE_TEST))
        bad_scanobs = session.query(schema.ScanTag.observation_id).filter(schema.ScanTag.type <0)

        #queries for OTFs, az and el-scans, use for filter in queuery for "source_fits"
        scn_qry = session.query(schema.ScanTag.observation_id).filter(schema.ScanTag.type == scn)


        source_fits = session.query(*tuple(cols.values()))\
            .filter(schema.PointSourceFit.observation_id == schema.Scan.observation_id, schema.PointSourceFit.scannumber == schema.Scan.scannumber) \
            .filter(schema.Scan.sourceid == schema.Source.id)\
            .filter(schema.PointSourceFit.observation_id == schema.Observation.id)\
            .filter(schema.Observation.receiver == rec)\
            .filter(schema.PointSourceFit.observation_id.notin_(bad_observations))\
            .filter(schema.PointSourceFit.observation_id.notin_(bad_scanobs))\
            .filter(schema.PointSourceFit.observation_id.in_(scn_qry))
            
        if ids is not None:
            source_fits = source_fits.filter(schema.PointSourceFit.observation_id.in_(ids))
        
        if manual_exclude_list is not None:
            source_fits = source_fits.filter(schema.PointSourceFit.observation_id.notin_(manual_exclude_list))
            
        if sources_excl_list is not None:
            source_fits = source_fits.filter(schema.Source.id.notin_(sources_excl_list))
            
        for filt in filters:
            source_fits = source_fits.filter(filt)
        
        
        source_fits = source_fits.order_by(sqlalchemy.desc(schema.Scan.starttime))
        
        yield source_fits.count()
        

        cols = list(cols.keys())
        for data in source_fits:
            dc = dict(zip(cols, data))
            dc['fname'] = '/data/output/' + dc['fname']
            yield dc



def load_hdf5(ff, full=True, weather=True):

    
    with h5py.File(ff, 'r') as file:

        # ============================
        # PREP
        # ============================

        pwr = np.array(file['scan/000/P0_ND0/power'])
        ra = np.unwrap(np.array(file['scan/000/P0_ND0/ra']), period=360)

        dec = np.array(file['scan/000/P0_ND0/dec'])
        t = np.array(file['scan/000/P0_ND0/timestamp']).flatten()
        dc = {
            'pwr': pwr,
            'ra': ra,
            'dec': dec,
            't': t
        }

        if full:
            dc['AZc'] = file['/monitor/acu/azimuth/p_point_corr'][:]
            dc['ELc'] = file['/monitor/acu/elevation/p_point_corr'][:]
            dc['Tc'] = Time(file['monitor/acu/actual_timestamp'][:], format='mjd').unix
            dc['AZci'] = interpolate.interp1d(dc['Tc'], dc['AZc'], fill_value="extrapolate")
            dc['ELci'] = interpolate.interp1d(dc['Tc'], dc['ELc'], fill_value="extrapolate")

            dc['beam_width'] = 1.22 * 3E8 / np.mean(file['scan/000/P0_ND0/frequency'][:]) / 12 * 180. / np.pi
        
        fields = 'air_pressure air_temperature humidity rain timestamp wind_direction wind_speed wind_speed_gusts'.split()
        
        if weather:
            dc['weather'] = {}
            for k in fields:
                vec = np.array(file['/monitor/weather/' + k])
                dc['weather'][k] = vec
                
        return dc
    