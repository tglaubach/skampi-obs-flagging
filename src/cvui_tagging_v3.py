import cv2
import numpy as np
import sqlite3
import io
from PIL import Image


import hashlib
import platform
import getpass

import pandas as pd
from astropy.time import Time


sqlite_file = r"data/NEW_tagging_db.sqlite"

n_cols = 16
n_imgs_max = n_cols*12


sysinfo = f"{getpass.getuser()} | {platform.uname().system} | {platform.uname().node}"


colors = {
    'yellow': (0, 255, 255),
    'cyan': (250,225,100),
    'mangenta': (255, 0, 255),
    'blue': (255, 178, 102),
    'beige': (102, 178, 255),
    'green': (0, 255, 0),
    'red': (0, 0, 255)
}

colors = {k:np.array(v, dtype=float)/255. for k, v in colors.items()}

class2color = {
    'good': 'green',
    'bad': 'red',
    'HIGH_RFI': 'mangenta',
    'LOW_SNR': 'blue',
    'NO_SRC': 'yellow',
    'BAD_SHAPE': 'beige'
}

classes = ['HIGH_RFI', 'LOW_SNR', 'NO_SRC', 'BAD_SHAPE', 'good']


def get_rows(select_sql = "SELECT * FROM observation_tagging;"):

    try:
        con = sqlite3.connect(sqlite_file)
        
        with con:
            df = pd.read_sql_query(select_sql, con)
            df = df.loc[:, ~df.columns.duplicated()]
            print(df.columns)
            return df.to_dict('records')        
    finally:
        if 'con' in locals() and con:
            con.close()

def update_rows(rows):
    
    sql = "UPDATE observation_tagging SET tags = ?, classifier = ?, clf_chksum = ?, time_changed = ?  WHERE obs_id = ?"
    cols = 'tags classifier clf_chksum time_changed obs_id'.split()

    recordList = [tuple([dc[c] for c in cols]) for dc in rows]
    
    try:
        con = sqlite3.connect(sqlite_file)
        with con:
            cursor = con.cursor()
            cursor.executemany(sql, recordList)
            cursor.close()
            con.commit()
        
    finally:
        if 'con' in locals() and con:
            con.close()
            


rows = get_rows('SELECT * FROM observation_tagging INNER JOIN rows on rows.obs_id = observation_tagging.obs_id ORDER BY scn_time_unx')


for i in range(len(rows)):
    rows[i]['image'] = np.array(Image.open(io.BytesIO(rows[i]['image'])), dtype=np.uint8)

def toggle_bad_good(event,x,y, flags,param):
    handle = False
    global full_img, rows, lut, slices, df_rows

    x0, y0 = int(x), int(y)
    i = lut[y0, x0]
    if i >= 0:
        r = rows[i]
        sx, sy = slices[i]
        obs_id = r['obs_id']
        class_i = r['tags']
        if event == cv2.EVENT_MOUSEWHEEL:
            class_offst = int(np.sign(flags))
            if r['classifier'] != 'manual':
                i_new_class = 0 if class_offst > 0 else len(classes)-1
                new_class_i = classes[i_new_class]
            else:
                i_class = classes.index(class_i)
                i_new_class = (i_class + class_offst) % len(classes)
                new_class_i = classes[i_new_class]
            handle=True
        elif event == cv2.EVENT_LBUTTONDOWN:
            new_class_i = 'LOW_SNR'
            handle=True
        elif event == cv2.EVENT_RBUTTONDOWN:
            new_class_i = 'BAD_SHAPE'
            handle=True
        elif event == cv2.EVENT_LBUTTONDBLCLK:
            new_class_i = 'NO_SRC'
            handle=True
        
        if handle:
            print(f'({x0, y0}) {i} | {obs_id} | {class_i} -> {new_class_i}')
            
            r['tags'] = new_class_i
            r['time_changed'] = Time.now().iso + 'Z'
            r['clf_chksum'] = 'manual'
            r['classifier'] = 'manual'
            r['sysinfo'] = sysinfo
            rows[i] = r
            full_img[sx, sy] = make_color(rows[i])
            # print(np.mean(img[:,:,0]), np.mean(img[:,:,1]), np.mean(img[:,:,2]))


def make_color(r):
    class_i = r['tags']
    img = r['image']
    obs_id = r['obs_id']
    clf = r['classifier']
    Tpk = Time(r['scn_time_unx'], format='unix')

    c_weights = colors[class2color[class_i]]
    
    i = np.zeros((img.shape[0], img.shape[1], 3), dtype=np.uint8)
    for ic in range(3):
        i[:,:,ic] = c_weights[ic] * img

    c = c_weights * 250
    suffix = (' m' if clf == 'manual' else '')

    txt = f'{class_i} {suffix}'

    i = cv2.putText(i, txt, (2,10), cv2.FONT_HERSHEY_PLAIN, fontScale=0.8, color=c)
    # st = (Tpk.iso + 'Z').replace('-', '').replace(':', '')

    s1 = Tpk.datetime.strftime("%y%m%d %H:%M")

    i = cv2.putText(i, f'{obs_id} {s1}', (2,img.shape[1]-4), cv2.FONT_HERSHEY_PLAIN, fontScale=0.5, color=c)
    # i = cv2.putText(i, s2, (2,img.shape[1]-5), cv2.FONT_HERSHEY_PLAIN, fontScale=0.8, color=c)

    return i


all_rows = [r for r in rows]
n, m = all_rows[0]['image'].shape

print('-'*100)
print(all_rows[0].keys())
print('-'*100)

irr = 0
while True:
    irrh = min(len(all_rows), irr + n_imgs_max)
    srr = slice(irr, irrh)
    print('srr', srr)
    rows = all_rows[srr]
    ii = int(irr/n_imgs_max)+1
    nn = int(np.ceil(len(all_rows)/n_imgs_max))
    img_name = f'IMAGE BATCH: {srr} / {len(all_rows)} [{ii}/{nn}]'


    n_rows = len(rows) / n_cols
    if n_rows % 1 > 0:
        n_rows = int(np.ceil(n_rows))
    else:
        n_rows = int(n_rows)

    print('n_rows', n_rows, len(rows) / n_cols)
    print(n, m, n_cols)
    full_img = np.zeros((n*n_rows, m*n_cols,3), dtype=np.uint8)
    lut = np.full((n*n_rows, m*n_cols), -1, dtype=np.uint16)
    slices = []

    i = 0
    for ir in range(n_rows):
        for ic in range(n_cols):
            if i >= len(rows):
                break

            sx = slice(ir*m, (ir+1)*m)
            sy = slice(ic*m, (ic+1)*m)
            
            slices.append((sx, sy))

            full_img[sx, sy] = make_color(rows[i])
            lut[sx, sy] = i

            i += 1

    
    cv2.namedWindow('image')
    cv2.setMouseCallback('image',toggle_bad_good)

    # print(full_img)
    while(1):
        cv2.imshow('image',full_img)
        cv2.setWindowTitle('image', img_name )

        k = cv2.waitKeyEx(100)
        command = ''
        if k == 27:
            command = 'quit'
        elif k == ord('q'):
            command = 'quit'
        elif k == ord('s'):
            command = 'save'

        elif k in [54, 2555904, ord('n'), ord('+')]:
            command = 'next'
        elif k in [52, 2424832, ord('l'), ord('-')]:
            command = 'last'
        
        if k != -1:
            print(k, command)

            if command == 'save':
                print('saving...')
                update_rows(rows)
                print('done')
            elif command == 'quit' or command == 'next' or command == 'last':
                break
                    

    if command == 'quit':
        break
    elif command == 'next':
        print('saving...')
        update_rows(rows)
        print('done')
        irr += n_imgs_max
    elif command == 'last':
        print('saving...')
        update_rows(rows)
        print('done')
        irr -= n_imgs_max
    
    if irr < 0:
        irr = 0
        while irr + n_imgs_max < len(all_rows):
            irr += n_imgs_max
    elif irr > len(all_rows):
        irr = 0

