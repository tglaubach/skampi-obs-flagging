
import numpy as np
from astropy.coordinates import SkyCoord, EarthLocation, AltAz, angular_separation, get_sun
import astropy.units as u
from astropy.time import Time


def calc_and_add_offsts(df, dish_location):
    df = df.copy()
    
    max_time = Time(df['pk_time'].values, format='unix')
    
    
    # transform of RADEC source position to altaz
    source_position_radec = SkyCoord(df['src_ra'].values*u.deg, 
                                     df['src_dec'].values*u.deg,frame='icrs', 
                                     obstime=max_time, location=dish_location)

    source_position_altaz = source_position_radec.transform_to('altaz')

    pk_az_prior = df['pk_az'].values - df['c_az'].values
    pk_el_prior = df['pk_el'].values - df['c_el'].values
    
    #calculate offset used to later create pointing model
    total_offset_az = pk_az_prior - source_position_altaz.az.value
    total_offset_el = pk_el_prior - source_position_altaz.alt.value

    total_offset_az_post = df['pk_az'].values - source_position_altaz.az.value
    total_offset_el_post = df['pk_el'].values - source_position_altaz.alt.value
    
    # rewrite the az offset, which has a 360 degree periodicity to be as close to zero as possible
    fun = lambda ang: np.where(abs(ang)>180.,ang-np.sign(ang)*360.,ang)
    
    total_offset_az = fun(total_offset_az)
    total_offset_az_post = fun(total_offset_az_post)
    
    d_ang_prior = angular_separation(source_position_altaz.az, source_position_altaz.alt, pk_az_prior * u.deg, pk_el_prior * u.deg)
    d_ang_post = angular_separation(source_position_altaz.az, source_position_altaz.alt, df['pk_az'].values * u.deg, df['pk_el'].values * u.deg)
    
    src_az = source_position_altaz.az.value
    src_el = source_position_altaz.alt.value
    
    d_az = -total_offset_az
    d_el = -total_offset_el
    
    d_az_post = -total_offset_az_post
    d_el_post = -total_offset_el_post
    
    d_ang_prior = d_ang_prior.to(u.deg).value
    d_ang_post = d_ang_post.to(u.deg).value
        
    
    df.insert(0, 'src_az', src_az)
    df.insert(1, 'src_el', src_el)

    df.insert(2, 'd_az', d_az)
    df.insert(3, 'd_el', d_el)
    
    df.insert(4, 'd_az_post', d_az_post)
    df.insert(5, 'd_el_post', d_el_post)

    df.insert(6, 'd_ang_prior', d_ang_prior)
    df.insert(7, 'd_ang_post', d_ang_post)
    return df
