import numpy as np

import random
import joblib


from scipy.stats import loguniform

from sklearn.model_selection import cross_val_predict
from sklearn.model_selection import RandomizedSearchCV

from sklearn.metrics import classification_report
from sklearn.metrics import ConfusionMatrixDisplay

from sklearn.preprocessing import StandardScaler, RobustScaler
from sklearn.decomposition import PCA
from sklearn.svm import SVC

from sklearn.pipeline import Pipeline

import matplotlib.pyplot as plt



def sel_obs(i_bad, i_good, ratio_req):
    if ratio_req is not None and ratio_req >= 0:
        p_bad, p_good = 1-(ratio_req/0.5)/2, (ratio_req/0.5)/2
        
        if p_bad > p_good:
            # want more bad than good

            p_req = p_good / p_bad

            n_bad = len(i_bad)
            n_good = int(n_bad * p_req)
            
            # not enough good 
            if n_good > len(i_good):
                n_good = len(i_good)
                n_bad = int(n_good / p_req)
            
            
        else:
            # want more good than bad            
            p_req = p_bad / p_good
            
            n_good = len(i_good)
            n_bad = int(n_good * p_req)
            
            
            # not enough bad 
            if n_bad > len(i_bad):
                n_bad = len(i_bad)
                n_good = int(n_bad / p_req)

        print('{:5.3f} | '.format(ratio_req) + f'n-BAD {n_bad} (avail: {len(i_bad)}) | n-GOOD {n_good} (avail: {len(i_good)})')

        assert len(i_bad) >= n_bad, "ERROR"
        assert len(i_good) >= n_good, "ERROR"

        print(' before: ', len(i_bad), len(i_good))
        i_bad = random.sample(i_bad, n_bad)
        i_good = random.sample(i_good, n_good)
        print('  after: ', len(i_bad), len(i_good))
        
    else:

        if len(i_bad) > len(i_good):
            i_bad = random.sample(i_bad, len(i_good))
        elif len(i_bad) < len(i_good):
            i_good = random.sample(i_good, len(i_bad))

    return i_bad, i_good



class otf_obs_classifier:

    def __init__(self, C=None, gamma=None, n_components=12, target_names=['bad', 'good'], use_robust_scaler=False) -> None:
        self.C = C
        self.gamma = gamma
        self.n_components = n_components
        self.target_names = target_names
        self.use_robust_scaler=use_robust_scaler

    def get_new_estimators(self, set_no_params=False):
        
        if self.use_robust_scaler:
            scaler = RobustScaler()
        else:
            scaler = StandardScaler()

        pca = PCA(n_components=self.n_components)
        

        if self.C is None or self.gamma is None or set_no_params:
            svc = SVC(kernel="rbf", class_weight="balanced")
        else:
            C = 1.0 if self.C is None else self.C
            gamma = 'scale' if self.gamma is None else self.gamma
            svc = SVC(kernel="rbf", class_weight="balanced", C=self.C, gamma=self.gamma)

        return {'rescale': scaler, 'reduce_dim': pca,'svm': svc}


        
    def find_hyper_params(self, X, y, n_iter=20, do_plot=True, do_set=False):
            
        (scaler, pca, svc) = self.get_new_estimators(set_no_params=True).values()

        X_fit = scaler.fit_transform(X)


        pca = pca.fit(X_fit)

        print("Projecting the input data on the orthonormal basis")

        X_fit = pca.transform(X_fit)


        print("Fitting hyper parameters")

        param_grid = {
            "C": loguniform(1e3, 1e5),
            "gamma": loguniform(1e-4, 1e-1),
        }
        clf = RandomizedSearchCV(svc, param_grid, n_iter=n_iter)

        clf = clf.fit(X_fit, y)

        print("Best estimator found by grid search:")
        print(clf.best_estimator_)


        gamma = clf.best_estimator_.gamma
        C = clf.best_estimator_.C

        if do_set:
            print("Setting new parameters:")
            print('   C     = ', C)
            print('   gamma = ', gamma)
            self.gamma=gamma
            self.C=C

        if do_plot:
            yf = clf.predict(X_fit)
            self.__plt(y, yf)
        
        return C, gamma

    def cross_val_fit(self, X, y, cv = 3, do_plot=True):

        estimators = self.get_new_estimators(set_no_params=False)

        pipe = Pipeline( list(estimators.items()) )

        yf = cross_val_predict(pipe, X, y, cv = 3)

        if do_plot:
            self.__plt(y, yf)

        return yf

    def fit(self, X, y, do_plot=True):
        estimators = self.get_new_estimators(set_no_params=False)

        pipe = Pipeline( list(estimators.items()) )
        pipe_fitted = pipe.fit(X, y)
        
        if do_plot:
            yf = pipe.predict(X)
            self.__plt(y, yf)

        return pipe_fitted


    def __plt(self, y, yf):
        print(classification_report(yf, y, target_names=self.target_names))
        ConfusionMatrixDisplay.from_predictions(y, yf, display_labels=self.target_names, xticks_rotation="vertical")
        plt.tight_layout()
        plt.show()


def load_pkl(pth):
    return joblib.load(pth)


def save_pkl(pth, pipe):
    joblib.dump(pipe, pth) 


def imgs2X(raw_in):
    # rescale 0...1
    imgs = [v - v.min() for v in raw_in]
    imgs = [i / np.max(i) for i in imgs]

    # extract raw features
    X = np.array([i.flatten() for i in imgs])
    return X

def run(raw_in, idx_all, idx_bad, obs_ratio = 0.45, n_components = 12, target_names = ['bad', 'good'], use_robust_scaler=False, do_plot=True, n_iter=20, cv=3):

    i_all = list(sorted(set(idx_all)))
    i_bad =  [i for i in list(idx_bad) if i in i_all]
    i_good = [i for i in i_all if i not in i_bad]

    print('n_all', len(i_all), 'n_bad', len(i_bad), 'n_good', len(i_good))

    i_bad, i_good = sel_obs(i_bad, i_good, obs_ratio)


    i_use = i_bad + i_good
    idx_use = np.array([i in i_use for i in i_all])


    assert np.sum(idx_use) == len(i_use), "something failed on the transformation for data prep"

    # extract raw features and target values
    X = imgs2X(raw_in)
    y = ~np.isin(i_all, i_bad) # 0 for BAD | 1 for good

    X = X[idx_use,:]
    y = y[idx_use]

    print(X.shape, y.shape)

    print('{} / {} | {:.1f} %'.format(np.sum(y), len(y), np.sum(y) / len(y) * 100))

    obj = otf_obs_classifier(n_components=n_components, target_names=target_names, use_robust_scaler=use_robust_scaler)

    obj.find_hyper_params(X, y, do_plot=do_plot, n_iter=n_iter, do_set=True)

    obj.cross_val_fit(X, y, cv=cv, do_plot=do_plot)

    pipe = obj.fit(X, y, do_plot=True)

    return pipe
