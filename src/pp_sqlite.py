
import sqlite3, os
import datetime
import pandas as pd
import numpy as np
import io
from astropy.time import Time

import hashlib
import platform
import getpass


verb = 1




sysinfo = f"{getpass.getuser()} | {platform.uname().system} | {platform.uname().node}"



def get_ids_done(sqlite_file, select_sql = """ SELECT obs_id FROM tagging;"""):
    
    if not os.path.exists(sqlite_file):
        return []
    
    try:
        con = sqlite3.connect(sqlite_file)
        
        with con:
            cursor = con.cursor()
            cursor.execute(select_sql)
            ids = cursor.fetchall()
            ids = [i[0] for i in ids]
            cursor.close()
            return ids
        
    finally:
        if 'con' in locals() and con:
            con.close()
            

def write_obs_tagging(sqlite_file, rows, set_info_auto=False):
    
    tablename = 'tagging'
    columns = {
        'obs_id': 'INTEGER PRIMARY KEY',
        'filename': 'TEXT NOT NULL',
        'rawdata': 'BLOB NOT NULL',
        
        'ra': 'BLOB not NULL',
        'dec': 'BLOB NOT NULL',
        'pwr': 'BLOB NOT NULL',
        'tags': 'TEXT',
        
        'classifier': 'TEXT', 
        'clf_chksum': 'TEXT', 

        'time_changed': 'TEXT',
        'sysinfo': 'TEXT'
    }
    # {'obs_id': <class 'str'>, 'filename': <class 'str'>, 'rawdata': <class 'bytes'>, 'ra': <class 'bytes'>, 'dec': <class 'bytes'>, 'pwr': <class 'bytes'>, 'tags': <class 'str'>, 'classifier': <class 'str'>, 'clf_chksum': <class 'str'>, 'time_changed': <class 'str'>, 'sysinfo': <class 'str'>}
    
    ct = ', '.join([k + ' ' + v for k, v in columns.items()])
    cts = ', '.join([k for k in columns.keys()])
    qms = ', '.join(['?']*len(columns))
    
    create_sql = f"CREATE TABLE IF NOT EXISTS {tablename} ({ct});"
    insert_sql = f"INSERT OR IGNORE INTO {tablename} ({cts}) VALUES ({qms});"

    if verb >= 2:
        print(create_sql)
        print(insert_sql)

    rr = []
    for r in rows:
        dc = {**r}
        if 'time_changed' not in dc or set_info_auto:
            dc['time_changed'] = Time.now().iso + 'Z'
        if 'sysinfo' not in dc or set_info_auto:
            dc['sysinfo'] = sysinfo
            
        assert dc.keys() == columns.keys(), f'rows must have equal keys to database. Expected: {columns.keys()}, but got {dc.keys()}'
        if not isinstance(dc['rawdata'], bytes):
            img_byte_arr = io.BytesIO()
            dc['rawdata'].save(img_byte_arr, format='PNG')
            dc['rawdata'] = img_byte_arr.getvalue()
        if isinstance(dc['tags'], list):
            dc['tags'] = ' '.join([str(t) for t in dc['tags']])
        else:
            dc['tags'] = str(dc['tags'])
            
        if not isinstance(dc['obs_id'], int):
            print(dc['obs_id'], type(dc['obs_id']))
            dc['obs_id'] = int(dc['obs_id'])
            
        tpl = tuple([dc[k] for k in columns.keys()])
                   
        if verb >= 2:
            print(tpl)
        # print([type(t) for t in tpl])
        rr.append(tpl)

    if verb >= 1:
        print((' '*6) + 'observation_tagging...')

    try:
        con = sqlite3.connect(sqlite_file)
        
        with con:
            cursor = con.cursor()
            cursor.execute(create_sql)
            cursor.executemany(insert_sql, rr)
            con.commit()
            cursor.close()

    finally:
        if 'con' in locals() and con:
            con.close()
            
def write_rows_pandas(sqlite_file, df, table_name, verb=1):

    if verb >= 1:
        print((' '*6) + table_name + '...')

    if isinstance(df, list):
        df = pd.DataFrame(df).set_index('obs_id')
    try:
        con = sqlite3.connect(sqlite_file)
        df.to_sql(table_name, con, index=True, if_exists='append')
    finally:
        if 'con' in locals() and con:
            con.close()
            
    
if __name__ == '__main__':

    sqlite_file = ":memory:"

    columns = {
        'obs_id': 1,
        'filename': 'TEXT NOT NULL',
        'rawdata': b'BLOB NOT NULL',
        
        'ra': b'BLOB not NULL',
        'dec': b'BLOB NOT NULL',
        'pwr': b'BLOB NOT NULL',
        'tags': 'TEXT',
        
        'classifier': 'TEXT', 
        'clf_chksum': 'TEXT', 

        'time_changed': 'TEXT',
        'sysinfo': 'TEXT'
    }

    write_obs_tagging(sqlite_file, [columns], True)


    columns = {
        'obs_id': 1,
        'filename': 'TEXT NOT NULL',
        'number': 1.1,
        'lala': 7, 
        'clf_chksum': 'TEXT', 
        'time_changed': 'TEXT',
        'sysinfo': 'TEXT'
    }

    write_rows_pandas(sqlite_file, [columns], 'testtable')