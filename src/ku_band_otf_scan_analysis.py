#idea: take hand-filtered OTF ku data and analyze
#gof and off source power fluctuations to see whether there is a correlation with
#manual acceptance / declining of data

from ska_proto.MSAproto.core import schema
from ska_proto.MSAproto.core.tags import TAGS
import matplotlib.pyplot as plt
import sqlalchemy
import numpy as np
from astropy.time import Time
from astropy.coordinates import SkyCoord, EarthLocation
import astropy.units as u
import sys
import h5py
from scipy.optimize import curve_fit

np.set_printoptions(threshold=sys.maxsize)
schema.init_db()

#hardcoded telescope position
latitude = -30.71797756  # degree
longitude = 21.41303794   # degree
height = 1053.0  #m above sea level
dish_location = EarthLocation(lat=latitude, lon=longitude, height=height)

off_distance = 0.15

#these are measurements of extended sources, they cannot be marked in the data bank currently
extended = [4586,4589,4604,4624,4681,5100,5103,5112,5118,5184,5194,5198,5199,5200,5205,5207,5208,5210,5213,5215,5216,5217,5227,5229,5232,5233,5239,5242,5251,5252,5261,5273,5274,5287,5289]

with schema.get_session() as session:
    
    #queries for filters; we dont want the "bad" observations with an error tag
    bad_observations = session.query(schema.ObservationTag.observation_id).filter(sqlalchemy.or_(schema.ObservationTag.type <0, schema.ObservationTag.type==TAGS.SOFTWARE_TEST))
    bad_scanobs = session.query(schema.ScanTag.observation_id).filter(schema.ScanTag.type <0)
    
    #query to find observations with tag ""-3" and ""-21"
    bad_fit_no_source_observations = session.query(schema.ObservationTag.observation_id).filter(sqlalchemy.or_(schema.ObservationTag.type == -3, schema.ObservationTag.type == -21))
   
    #queries for OTFs, az and el-scans, use for filter in queuery for "source_fits"
    OTF = session.query(schema.ScanTag.observation_id).filter(schema.ScanTag.type == 2)
    az = session.query(schema.ScanTag.observation_id).filter(schema.ScanTag.type == 3)
    el = session.query(schema.ScanTag.observation_id).filter(schema.ScanTag.type == 4)
    
    #query to get the OTF fits of the latest epoch
    #use filter(schema.Observation.receiver == 3) for S-Band and ...==2 for Ku
    source_fits = session.query(schema.PointSourceFit.data['peak']['amplitude'],
                               schema.PointSourceFit.data['peak']['position']['az'],
                                schema.PointSourceFit.data['peak']['position']['el'],
                                schema.PointSourceFit.data['peak']['applied_correction']['az'],
                                schema.PointSourceFit.data['peak']['applied_correction']['el'],
                                schema.PointSourceFit.data['peak']['time'],
                                schema.PointSourceFit.data['peak']['background'],                                
                                schema.Source.flux,
                                schema.Source.id,
                                schema.Scan.scannumber,                                
                                schema.PointSourceFit.observation_id,
                                schema.Scan.starttime,
                                schema.PointSourceFit.data['peak_fit']['gof'],
                                schema.Source.ra,
                                schema.Source.dec,
                                schema.Observation.observation_epoch,
                                schema.Observation.filename
                               )\
    .filter(schema.PointSourceFit.observation_id == schema.Scan.observation_id, schema.PointSourceFit.scannumber == schema.Scan.scannumber) \
    .filter(schema.Scan.sourceid == schema.Source.id).filter(schema.PointSourceFit.observation_id == schema.Observation.id)\
    .filter(schema.Observation.receiver == 2)\
    .filter(schema.Observation.observation_epoch == 9)\
    .filter(schema.PointSourceFit.observation_id.notin_(bad_observations))\
    .filter(schema.PointSourceFit.observation_id.notin_(bad_scanobs))\
    .filter(schema.PointSourceFit.observation_id.in_(OTF))\
    .filter(schema.PointSourceFit.version==1)\
    .filter(sqlalchemy.or_(sqlalchemy.and_(schema.PointSourceFit.observation_id>5100,schema.PointSourceFit.observation_id<5300),sqlalchemy.and_(schema.PointSourceFit.observation_id>4500,schema.PointSourceFit.observation_id<4700)))
    
    #selecting data with tags "-3" and "-21"
    #this could be done much more beautyful and shorter, but I just copy-pastad this for now
    bad_fits_no_source = session.query(schema.PointSourceFit.data['peak']['amplitude'],
                               schema.PointSourceFit.data['peak']['position']['az'],
                                schema.PointSourceFit.data['peak']['position']['el'],
                                schema.PointSourceFit.data['peak']['applied_correction']['az'],
                                schema.PointSourceFit.data['peak']['applied_correction']['el'],
                                schema.PointSourceFit.data['peak']['time'],
                                schema.PointSourceFit.data['peak']['background'],                                
                                schema.Source.flux,
                                schema.Source.id,
                                schema.Scan.scannumber,                                
                                schema.PointSourceFit.observation_id,
                                schema.Scan.starttime,
                                schema.PointSourceFit.data['peak_fit']['gof'],
                                schema.Source.ra,
                                schema.Source.dec,
                                schema.Observation.observation_epoch,
                                schema.Observation.filename
                               )\
    .filter(schema.PointSourceFit.observation_id == schema.Scan.observation_id, schema.PointSourceFit.scannumber == schema.Scan.scannumber) \
    .filter(schema.Scan.sourceid == schema.Source.id).filter(schema.PointSourceFit.observation_id == schema.Observation.id)\
    .filter(schema.Observation.receiver == 2)\
    .filter(schema.Observation.observation_epoch == 9)\
    .filter(schema.PointSourceFit.observation_id.in_(bad_fit_no_source_observations))\
    .filter(schema.PointSourceFit.observation_id.notin_(bad_scanobs))\
    .filter(schema.PointSourceFit.observation_id.in_(OTF))\
    .filter(schema.PointSourceFit.version==1)\
    .filter(sqlalchemy.or_(sqlalchemy.and_(schema.PointSourceFit.observation_id>5100,schema.PointSourceFit.observation_id<5300),sqlalchemy.and_(schema.PointSourceFit.observation_id>4500,schema.PointSourceFit.observation_id<4700)))
    
    
    
    
    #saving multiple lines of the query into numpy arrays
    
    azimuth = np.array(np.array(source_fits[:])[:,1],dtype=float)
    elevation = np.array(np.array(source_fits[:])[:,2],dtype=float)
    c_azimuth = np.array(np.array(source_fits[:])[:,3],dtype=float)
    c_elevation = np.array(np.array(source_fits[:])[:,4],dtype=float)
    source_ra = np.array(np.array(source_fits[:])[:,13],dtype=float)
    source_dec = np.array(np.array(source_fits[:])[:,14],dtype=float)
    max_time = Time(np.array(source_fits[:],dtype=str)[:,5],format='unix')
    obs_id = np.array(source_fits[:])[:,10]
    source_id = np.array(source_fits[:])[:,8] 
    gof = np.array(np.array(source_fits[:])[:,12],dtype=float)
    filenames = np.array(source_fits[:],dtype=str)[:,16]
    
    #bad means explicitly tags "-3" and "-21"
    gof_bad = np.array(np.array(bad_fits_no_source[:])[:,12],dtype=float)
    max_time_bad = Time(np.array(bad_fits_no_source[:],dtype=str)[:,5],format='unix')
    filenames_bad = np.array(bad_fits_no_source[:],dtype=str)[:,16]
    source_ra_bad = np.array(np.array(bad_fits_no_source[:],dtype=str)[:,13],dtype=float)
    source_dec_bad = np.array(np.array(bad_fits_no_source[:],dtype=str)[:,14],dtype=float)
    obs_id_bad = np.array(np.array(bad_fits_no_source[:],dtype=str)[:,10],dtype=float)

    
    #trafo of source position to altaz
    source_position_radec = SkyCoord(source_ra*u.deg,source_dec*u.deg,frame='icrs',obstime=max_time,location=dish_location)
    source_position_altaz = source_position_radec.transform_to('altaz')
    source_az = source_position_altaz.az.value
    source_el =source_position_altaz.alt.value

    total_offset_az =  (azimuth - source_az + c_azimuth)
    
idx = np.zeros(len(gof),dtype=bool)
for i in range(len(gof)):
    if obs_id[i] not in extended:
        idx[i] = True
        
idx_bad = np.zeros(len(gof_bad),dtype=bool)
for i in range(len(gof_bad)):
    if obs_id_bad[i] not in extended:
        idx_bad[i] = True        
    

def lin_plane(data, a, b, c):
    return data[:,0]*a + data[:,1]*b + c

power_off_std_list_good=[]
power_off_mean_list_good=[]

#loop over good data
for i in range(len(filenames[idx])):
    #print(i/len(filenames))
    filename = (filenames[idx])[i]
    file = h5py.File("data/output/"+filename)
    az_0 = np.array(file['scan/000/P0_ND0/azimuth'])
    el_0 = np.array(file['scan/000/P0_ND0/elevation'])
    ra_0 = np.array(file['scan/000/P0_ND0/ra'])
    dec_0 = np.array(file['scan/000/P0_ND0/dec'])
    power = np.array(file['scan/000/P0_ND0/power'])
    idx_off = np.where((source_ra[i]-ra_0)**2+(source_dec[i]-dec_0)**2>0.15**2)

    guess = (1e27,1e27,1e27)
    az_el = np.transpose(np.vstack((az_0[idx_off],el_0[idx_off])))
    [a,b,c], pcov = curve_fit(lin_plane, az_el, power[idx_off], guess)
    power_off_mean = np.mean(power[idx_off])
    power_off_std = np.std(power[idx_off]-lin_plane(az_el,a,b,c))
    power_off_mean_list_good.append(power_off_mean)    
    power_off_std_list_good.append(power_off_std)
    #print(filename)
    #print((power[idx_off])[0])
    
power_off_std_list_bad=[]
power_off_mean_list_bad=[]
for i in range(len(filenames_bad[idx_bad])):
    #print(i/len(filenames_bad))
    filename = (filenames_bad[idx_bad])[i]
    file = h5py.File("data/output/"+filename)
    az_0 = np.array(file['scan/000/P0_ND0/azimuth'])
    el_0 = np.array(file['scan/000/P0_ND0/elevation'])
    ra_0 = np.array(file['scan/000/P0_ND0/ra'])
    dec_0 = np.array(file['scan/000/P0_ND0/dec'])
    power = np.array(file['scan/000/P0_ND0/power'])
    idx_off = np.where((source_ra_bad[i]-ra_0)**2+(source_dec_bad[i]-dec_0)**2>0.15**2)

    guess = (1e27,1e27,1e27)
    az_el = np.transpose(np.vstack((az_0[idx_off],el_0[idx_off])))
    [a,b,c], pcov = curve_fit(lin_plane, az_el, power[idx_off], guess)
    power_off_mean = np.mean(power[idx_off])
    power_off_std = np.std(power[idx_off]-lin_plane(az_el,a,b,c))
    power_off_mean_list_bad.append(power_off_mean)    
    power_off_std_list_bad.append(power_off_std)  
#print(len(ra[idx_off]))
#print(len(ra[idx_on]))



#determine indizes for day and night
idx_good_night = np.where((max_time.mjd%1<0.1666666) | (max_time.mjd%1>0.8333333))
idx_good_day = np.where((max_time.mjd%1>0.3333333) & (max_time.mjd%1<0.6666666))
idx_bad_night = np.where((max_time_bad.mjd%1<0.1666666) | (max_time_bad.mjd%1>0.8333333))
idx_bad_day = np.where((max_time_bad.mjd%1>0.3333333) & (max_time_bad.mjd%1<0.6666666))
time_of_day_good = max_time.mjd%1
time_of_day_bad = max_time_bad.mjd%1


#make plot for gof and off_source flucutation
plt.xscale('log')
plt.yscale('log')
plt.xlabel("power_off std/power_off_mean")
plt.ylabel("gof/power_off_mean")
plt.scatter(np.array(power_off_std_list_good)/np.array(power_off_mean_list_good),np.array(gof)/np.array(power_off_mean_list_good),label="data_good",alpha=0.3)
plt.scatter(np.array(power_off_std_list_bad)/np.array(power_off_mean_list_bad),np.array(gof_bad)/np.array(power_off_mean_list_bad),label="data_bad",alpha=0.3)
plt.legend()
plt.show()

#make overview plot for distribution of data over time of day
plt.xlabel("time of day")
plt.scatter(time_of_day_good*24.,np.zeros(len(time_of_day_good)),s=0.5)
plt.scatter(time_of_day_bad*24.,np.ones(len(time_of_day_bad)),s=0.5)
plt.show()

#Print numbers of observations day and night
print("# Good_measurements Night:" + str(len(time_of_day_good[idx_good_night])))
print("# Bad measurements Night:" + str(len(time_of_day_bad[idx_bad_night])))
print("# Good measurements Day:" + str(len(time_of_day_good[idx_good_day])))
print("# Bad measurements Day:" + str(len(time_of_day_bad[idx_bad_day])))

print("finished")
